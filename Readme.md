# Project Name: *BlakJack*

## Implementation:
  * HTML
  * CSS
  * JS

## Premise:
  * Design a basic Browser Black jack game

## Execution requirements:
  * Any web browser- preferable *Chrome*- as other browsers have not been extensively tested

## Core Features
  * CF1 : Display start message
  * CF2 : Dislay player cards
  * CF3 : Display sum of player cards
  * CF4 : Update player cards at start of game
  * CF5 : Design a start button


## Addtional Features
  * AF1 : Display session total tally
  *

## Completion:

  * Display count and save buttons : *100 %*
  * Increment count on click : *100 %*
  * Save current count : *100 %*
  * Display current tally : *100 %*
  * Display previous saved tallies : *100 %*



## Optional Completions:
  *
